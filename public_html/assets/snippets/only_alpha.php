<?php
$output = false;

if (preg_match('/^[A-Zа-я _-]+$/iu', $value)) {
    $output = true;
} else {
    $validator->addError($key, 'Только буквы, тире и пробел');
    $output = false;
}

return $output;