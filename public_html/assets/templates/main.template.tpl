<!doctype html>
<html lang="en">
<head>
    [[$Meta]]
    [[$Main_style]]
    
</head>
<body>
    
    [[$Header]]

    <section class="top-form">
        <div class="container">
            <div class="row">
                <div class="d-none d-md-flex col-md-12 d-lg-none md-image"></div>
                <div class="d-none d-md-flex col-md-1 d-lg-none"></div>
                <div class="col-12 col-md-10 col-lg-6">
                    <h1 class="header">Работа водителем такси</h1>
                        [[!AjaxForm?
                            &snippet                = `FormIt`
                            &form                   = `form.main.template.tpl`
                            &hooks                  = `email`
                            &emailTpl               = `form.main.email.tpl`
                            &emailSubject           = `Сообщение с сайта [[++site_name]]`
                            &emailTo                = `rksema.ooo@yandex.ru`
                            &validate               = `name:required:only_alpha:maxLength=^127^,phone:required:isNumber:minLength=^6^:maxLength=^11^,city:required:only_alpha:minLength=^2^:maxLength=^127^,policy:required,message:maxLength=^1000^`
                            &customValidators       = `only_alpha`
                            &vTextRequired          = `Заполните это поле`
                            &vTextIsNumber          = `Введите только цифры`
                            &validationErrorMessage = `В форме содержатся ошибки!`
                            &successMessage         = `Ваша заявка успешно отправлена.`
                            &clearFeildsOnSuccess   = `true`
                        ]]
                </div>
                <div class="d-none d-md-flex col-md-1 d-lg-none"></div>
            </div>
        </div>
    </section>

    <section class="comfort">
        <div class="container">
            <div class="row">
                <div class="col-12 header">Удобно и выгодно</div>
                [[!getImageList?
                    &tpl    = `comfort.tpl`
            	    &tvname = `comfort` 
                ]]
            </div>
        </div>
    </section>

    <section class="begin">
        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="col-12 head">
                        Мгновенные выплаты нашим водителям <a href="https://myyandex-online.ru/" target="_blank">my.yandex-online.ru</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="header">
                        Как начать работу
                    </div>
                    <ul class="list-unstyled">
                        <li><span>1.</span> Оставьте заявку</li>
                        <li><span>2.</span> Мы позвоним вам, чтобы уточнить детали</li>
                        <li><span>3.</span> Выходите на линию и начинайте зарабатывать</li>
                    </ul>
                </div>
                <div class="d-none d-sm-block col-md-6 image-bg"></div>
            </div>
        </div>
    </section>

    <section class="taxometer">
        <div class="container">
            <div class="row">
                <div class="col-12 header">Таксометр</div>
                [[!getImageList?
                    &tpl    = `comfort.tpl`
            	    &tvname = `taxometer` 
                ]]
            </div>
        </div>
    </section>

    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="col-12 header">Свяжитесь с нами</div>
                <div class="col-12 col-md-5">
                    По телефону или онлайн
                    <div class="email"><img src="/assets/images/mail-box.png"> <a href="mailto:[[++email]]">[[++email]]</a></div>
                    <div class="phone"><img src="/assets/images/phone-box.png"> <a href="tel:[[++phone_code]]" onclick="ym(56081062,'reachGoal','TELEFON');return true;">[[++phone_view]]</a></div>
                </div>
                <div class="col-12 col-md-7">
                        [[!AjaxForm?
                            &snippet                = `FormIt`
                            &form                   = `form.main.template.tpl`
                            &hooks                  = `email`
                            &emailTpl               = `form.main.email.tpl`
                            &emailSubject           = `Сообщение с сайта [[++site_name]]`
                            &emailTo                = `rksema.ooo@yandex.ru`
                            &validate               = `name:required:only_alpha:maxLength=^127^,phone:required:isNumber:minLength=^6^:maxLength=^11^,city:required:only_alpha:minLength=^2^:maxLength=^127^,policy:required,message:maxLength=^1000^`
                            &customValidators       = `only_alpha`
                            &vTextRequired          = `Заполните это поле`
                            &vTextIsNumber          = `Введите только цифры`
                            &validationErrorMessage = `В форме содержатся ошибки!`
                            &successMessage         = `Ваша заявка успешно отправлена.`
                            &clearFeildsOnSuccess   = `true`
                        ]]
                </div>
                <div class="col-12">
                    [[*notices]]
                </div>
            </div>
        </div>
    </section>

    [[$Footer]]
    [[$Javascript]]

</body>
</html>
